const fs = require('fs')

function getInput(filename) {
  return fs.readFileSync(filename).toString()
}

function filter(input) {
  return input.replace('+', '')
}

function calculatePart1 (input) {
  return input
          .split('\n')
          .map(filter)
          .map(Number)
          .reduce((acc, num) => acc + num)
}

const cycles = {}
let acc = 0

function calculatePart2 (input) {
  const data = input.split('\n').map(filter).map(Number)

  for (number of data) {
    let sum = acc + number
    acc = sum
    if (cycles[sum] === undefined) {
      cycles[sum] = 1
    } else {
      return sum
    }
  }
}

module.exports = function () {
  const data = getInput('./day1/input.txt')
  console.log('Day 1 — Part 1: ', calculatePart1(data))

  let answer = undefined

  while (answer === undefined) {
    answer = calculatePart2(data)
  }

  console.log('Day 1 — Part 2: ', answer)
}