const fs = require('fs')

function sortDates (date1, date2) {}

function sortGuards (guard1, guard2) {
  return guard1.timeAsleep < guard2.timeAsleep
}

class Guard {
  constructor (id) {
    this.id = id
    this.timeAsleep = 0
    this.timesAsleep = 0
    this.start = []
    this.end = []
    this.overlaps = []
    this.intervals = []
  }

  registerSleepStart (time) {
    let [ _, hour ] = time.split(' ')
    this.timesAsleep += 1
    this.start.push(hour.split(':'))
  }

  registerSleepEnd (time) {
    let [_, hour] = time.split(' ')
    this.end.push(hour.split(':'))
    this.calculateTimeAsleep()
  }

  calculateTimeAsleep () {
    let start = Number(this.start[this.start.length - 1][1])
    let end = Number(this.end[this.end.length - 1][1])

    this.timeAsleep += (end - start)
  }

  calculateOverlap () {
    for (let i = 0; i < this.start.length; i++) {
      let start = this.start[i][1]
      let end = this.end[i][1]

      for (let j = start; j <= end; j++) {
        if (this.intervals.indexOf(j) >= 0) {
          this.overlaps.push(j - 1)
        } else {
          this.intervals.push(j)
        }
      }
    }
  }
}

function getId (line) {
  const splitLine = line.split('#')
  if (splitLine.length == 1) {
    return false
  } else {
    return splitLine[1].split(' ')[0]
  }
}

function fallsAsleep (line) {
  return line.indexOf('falls asleep') >= 0
}

function wakesUp (line) {
  return line.indexOf('wakes up') >= 0
}

function guardExists (id) {
  return guards[id] !== undefined
}

const guards = {}

function calculateAnswer (input) {
  let currentGuard = undefined
  for (line of input) {
    let [time, action] = line
    let id = getId(action)

    if (guardExists(id)) {
      currentGuard = guards[id]
    } else if (fallsAsleep(action)) {
      currentGuard.registerSleepStart(time)
    } else if (wakesUp(action)) {
      currentGuard.registerSleepEnd(time)
    } else if (id !== undefined) {
      let guard = new Guard(id)
      guards[id] = guard
      currentGuard = guard
    }
  }

  let guardArray = Object.values(guards)
  let guardLongestAsleep = guardArray.sort(sortGuards)[0]
  guardLongestAsleep.calculateOverlap()
  return guardLongestAsleep.overlaps.pop() * guardLongestAsleep.id
}

function formatInput(data) {
  return data.split('\n').map(line => line.split(']'))
}

module.exports = function () {
  let data = fs.readFileSync('./day4/input2.txt').toString()
  data = formatInput(data)
  console.log('Day 4 — Part 1:', calculateAnswer(data))
}