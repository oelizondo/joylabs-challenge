## Joylabs Coding Challenge

### Installing and running the project:

Make sure you have Node.js installed on your machine.

1. `git clone https://gitlab.com/oelizondo/joylabs-challenge.git && cd joylabs-challenge`
2. `npm start`

### What's missing:

#### Completed problems:

- [x] Day 1 Part 1 
- [x] Day 1 Part 2
- [x] Day 2 part 1
- [x] Day 2 part 2
- [x] Day 4 part 1
- [ ] Day 4 part 2

I finished the first to days (challenges) without too much problem. The fouth challenge was solved half way due to time constraints. I managed to get the correct answer (based on the sample input in the problem's description), but not to sort the dates correctly to handle the problem's formal input.

### Author

Oscar Elizondo