const fs = require('fs')

function mapToHash (hash, letter) {
  if (hash[letter] === undefined) {
    hash[letter] = 1
  } else {
    hash[letter] += 1
  }
}

const occurences = { two: 0, three: 0}

function getSetValues (word) {
  const hash = {}
  word.split('').forEach(mapToHash.bind(null, hash))
  let values = [...new Set(Object.values(hash))].filter(v => v === 2 || v === 3)
  return values
}

function mapOccurences (values) {
  values.forEach(n => n === 3 ? occurences['three'] += 1 : occurences['two'] += 1)
}

function scanIds (input) {
  input
    .split('\n')
    .map(getSetValues)
    .map(mapOccurences)

  return occurences['three'] * occurences['two']
}

function filterIds(input) {
  let words = input.split('\n')
  let differences = 0
  let intersections = []

  for (let i = 0; i < words.length; i++) {
    let word = words[i].split('')

    for (let j = 0; j < words.length; j++) {
      let word2 = words[j].split('')

      for(let k = 0; k < word.length; k++) {
        if (word[k] != word2[k]) {
          differences += 1
        } else {
          intersections.push(word[k])
        }
      }

      if (differences === 1) {
        return intersections.join('')
      } else {
        intersections = []
        differences = 0
      }
    }
  }

  return intersections
}

module.exports = function () {
  let data = fs.readFileSync('./day2/input.txt').toString()
  let answer = scanIds(data)
  console.log('Day 2 — Part 1:', answer)
  answer = filterIds(data)
  console.log('Day 2 — Part 2:', answer)
}