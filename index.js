const runDay1 = require('./day1/day1')
const runDay2 = require('./day2/day2')
const runDay4 = require('./day4/day4')

runDay1()
runDay2()
runDay4()